<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class TestController extends Controller
{
    public $omnivoreGeneral;
    public $omnivoreTickets;
    public $omnivoreTables;

    public function index(Request $request)
    {
        $locationId  = env('OMNIVORE_LOCATION_ID');

        $ticketList  = Omnivore::tickets()->ticketList($locationId);

        $tables      = Omnivore::tables()->tableList($locationId);

        $locations   = Omnivore::general()->locationList();

        dd(compact('ticketList','tables','locations'));

        dd($ticketList);
        return response()->json($ticketList);

//        dd($request->all());
    }
}
